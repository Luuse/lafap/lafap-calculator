<?php 
foreach ($containers as $container){ 
    $container_title = $container->title;
    $container_name = $container->name;
    $container_column = $container->get('colonne');
    $container_row = $container->get('rangee');

    if ($container_name == 'calculateur'){
        echo '<section class="container clicked" id="' . $container_name . '" data-row="' . $container_row .'" data-column="' . $container_column . '">';
    } else {
        echo '<section class="container" id="' . $container_name . '" data-row="' . $container_row .'" data-column="' . $container_column . '">';
    }
    
    $parts = $container->children();
    $j = 1;
    foreach ($parts  as $part){
        $calculateur = ($part->template == 'calculateur_artiste' ? true : $part->template == 'calculateur_curateurice') ? true : false;
        $feuillets = $part->template == 'feuillet';
        $feuillet_name = $part->name;
        $feuillet_id = $container_name. '_' .$feuillet_name;

        if (count($parts) > 1) {
            if ($j == 1) {
                echo '<input type="radio" data-id="' .$j. '" name="' .$container_name. '" id="' .$feuillet_id. '" value="' .$feuillet_id. '" checked>';
            }else if($j > 1){
                echo '<input type="radio" data-id="' .$j. '" name="' .$container_name. '" id="' .$feuillet_id. '" value="' .$feuillet_id. '">';
            }
            $j++;
        }
        

        if ($calculateur == true) {
            include('calculateur.php');
        } else {
            ?>
            <div class="feuille">
                <h1 class="hidden"><?= $container->getLanguageValue($lang, 'title'); ?></h1>
                <h1><?= $container->getLanguageValue($lang, 'title'); ?></h1>
                <article>
                    <?php
                    if ($lang == 'fr' || $lang == 'default') {
                        echo rft_fr($part->getLanguageValue($lang, 'text')); 
                    } else {
                        echo rft_en($part->getLanguageValue($lang, 'text'));
                    }
                    ?>
                </article>
            </div>
            <?php
        }
    } ?>
    <section class="feuillet" id="<?= $part->id ?>">
        <?php
        $i = 1;
        if (count($parts) > 1) {
            foreach ($parts  as $part){
                $part_title = $part->getLanguageValue($lang, 'title');
                $part_name = $part->name;
                $part_id = $container_name. '_' .$part_name;
                echo '<label data-id="' .$i. '" for="' .$part_id. '">';
                    echo $part_title;
                echo '</label>';
                $i++;
            } 
        } ?>
    </section>
</section>
<?php }