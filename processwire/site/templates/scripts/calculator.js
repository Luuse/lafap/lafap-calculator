var calculatorArtiste = document.querySelector(".calculator.artiste");
var tauxArtiste = calculatorArtiste.querySelector(".taux");
var tauxInputsArtiste = tauxArtiste.querySelectorAll("input[type=number]");
var tauxInputsMediationArtiste =
  calculatorArtiste.querySelectorAll("input[type=number]");
var tauxJournalierArtiste =
  calculatorArtiste.querySelector("#taux-journalier").value;
var tauxJournalierMediationArtiste = calculatorArtiste.querySelector(
  "#taux-journalier-mediation"
).value;
var prixSigneArtiste = calculatorArtiste.querySelector("#prix-signe").value;
var droitsExpo = calculatorArtiste.querySelector(".droits");
var droitsInput = droitsExpo.querySelectorAll("input[type=number]");
var totalDroitsExpoEl = droitsExpo.querySelector(".total .price span");
var surface = calculatorArtiste.querySelector("#surface-expo").value;
var nbArtistes = calculatorArtiste.querySelector("#nombre-artiste").value;
var nouvellesPieces =
  calculatorArtiste.querySelector("#nouvelles-pieces").value;
var duree = calculatorArtiste.querySelector("#duree-evenement").value;
var totalDroitsExpo = 0;
var conceptionArtiste = calculatorArtiste.querySelector(".conception");
var mediationArtiste = calculatorArtiste.querySelector(".mediation");
var textesArtiste = calculatorArtiste.querySelector(".textes");
var totalTextesArtiste = 0;
var horsRenumerationArtiste =
  calculatorArtiste.querySelector(".horsRenumeration");
var totalHorsRemunerationArtiste = 0;

var calculatorCurateurice = document.querySelector(".calculator.curateurice");
var tauxCurateurice = calculatorCurateurice.querySelector(".taux");
var tauxInputsCurateurice =
  tauxCurateurice.querySelectorAll("input[type=number]");
var tauxJournalierConceptionCurateurice =
  calculatorCurateurice.querySelector("#taux-journalier");
var tauxJournalierMediationCurateurice = calculatorCurateurice.querySelector("#taux-journalier-mediation").value;
var prixSigneCurateurice = calculatorCurateurice.querySelector("#prix-signe").value;
var conceptionCurateurice = calculatorCurateurice.querySelector(".conception");
var montageCurateurice = calculatorCurateurice.querySelector(".montage");
var mediationCurateurice = calculatorCurateurice.querySelector(".mediation");
var textesCurateurice = calculatorCurateurice.querySelector(".textes");
var totalTextesCurateurice = 0;
var horsRenumerationCurateurice = calculatorCurateurice.querySelector(".horsRenumeration");
var totalHorsRemunerationCurateurice = 0;

var firstArtiste = true;
var firstCurateurice = true;
var alertCurateurice = calculatorCurateurice.querySelector('.alertCurateurice');


tauxInputsArtiste.forEach((input) => {
  getTaux(input);
  sumDroitsExpo(droitsInput, conceptionArtiste, firstArtiste);
  sumDays(conceptionArtiste, 10, 5, firstArtiste);
  sumDays(mediationArtiste, null, null, firstArtiste);
  sumTexts(textesArtiste, firstArtiste);
  sumHorsRenum(horsRenumerationArtiste, firstArtiste);
  firstArtiste = false;
  input.addEventListener("input", function (e) {
    alertCurateurice.classList.remove('visible');
    getTaux(input);
    sumDroitsExpo(droitsInput, conceptionArtiste, firstArtiste);
    sumDays(conceptionArtiste, 10, 5, firstArtiste);
    sumDays(mediationArtiste, null, null, firstArtiste);
    sumTexts(textesArtiste, firstArtiste);
  });
});

tauxInputsCurateurice.forEach((input) => {
  getTaux(input);
  sumDays(conceptionCurateurice, null, 1, firstCurateurice);
  sumDays(mediationCurateurice, null, null, firstCurateurice);
  sumTexts(textesCurateurice, firstCurateurice);
  sumHorsRenum(horsRenumerationCurateurice, firstCurateurice);
  firstCurateurice = false;
  input.addEventListener("input", function (e) {
    getTaux(input);
    sumDays(conceptionCurateurice, 10, 5, firstCurateurice);
    sumDays(mediationCurateurice, null, null, firstCurateurice);
    sumTexts(textesCurateurice, firstCurateurice);
  });
});

var printButtonArtiste = calculatorArtiste.querySelector(".print");
var printButtonCurateurice = calculatorCurateurice.querySelector(".print");

printButtonArtiste.addEventListener("click", function () {
  window.print();
});

printButtonCurateurice.addEventListener("click", function () {
  window.print();
});

function getTaux(input) {
  var parent = input.closest("article");
  var inputId = input.id;
  var inputValue = input.valueAsNumber;
  input.classList.remove("alert");
  if (isNaN(inputValue) != true) {
    if (parent.classList.contains("artiste")) {
      if (inputId == "taux-journalier") {
        tauxJournalierArtiste = inputValue;
      } else if (inputId == "taux-journalier-mediation") {
        tauxJournalierMediationArtiste = inputValue;
      } else if (inputId == "prix-signe") {
        prixSigneArtiste = inputValue;
      }
    } else if (parent.classList.contains("curateurice")) {
      if (inputId == "taux-journalier") {
        tauxJournalierConceptionCurateurice = inputValue;
      } else if (inputId == "taux-journalier-mediation") {
        tauxJournalierMediationCurateurice = inputValue;
      } else if (inputId == "prix-signe") {
        prixSigneCurateurice = inputValue;
      }
    }
  } else {
    alertNaN(input);
  }
}

function sumDroitsExpo(droitsInput, conception, first) {
  droitsInput.forEach((input) => {
    updateDroitsExpo(input, conception);
    if (first == true) {
      input.addEventListener("input", function (e) {
        updateDroitsExpo(input, conception);
      });
    }
  });
}

function updateDroitsExpo(input, conception) {
  var content = input.valueAsNumber;
  if (isNaN(content) != true) {
    var inputId = input.id;
    input.classList.remove("alert");
    if (inputId == "surface-expo") {
      if (content < 5) {
        surface = 5;
      } else if (content >= 5 && content <= 10) {
        surface = 6;
      } else if (content >= 11 && content <= 20) {
        surface = 7;
      } else if (content >= 21 && content <= 30) {
        surface = 8;
      } else if (content >= 31 && content <= 40) {
        surface = 9;
      } else if (content >= 41 && content <= 50) {
        surface = 10;
      } else if (content >= 51 && content <= 60) {
        surface = 11;
      } else if (content >= 61 && content <= 70) {
        surface = 12;
      } else if (content >= 71 && content <= 80) {
        surface = 13;
      } else if (content >= 81 && content <= 90) {
        surface = 14;
      } else if (content >= 91 && content <= 100) {
        surface = 15;
      } else if (content >= 101 && content <= 110) {
        surface = 16;
      } else if (content >= 111 && content <= 120) {
        surface = 17;
      } else if (content >= 121 && content <= 130) {
        surface = 18;
      } else if (content >= 131 && content <= 140) {
        surface = 19;
      } else if (content >= 141 && content <= 150) {
        surface = 20;
      } else if (content >= 151 && content <= 170) {
        surface = 22;
      } else if (content >= 171 && content <= 190) {
        surface = 24;
      } else if (content >= 191 && content <= 210) {
        surface = 26;
      } else if (content >= 211 && content <= 230) {
        surface = 28;
      } else if (content > 230) {
        surface = 30;
      }
      console.log(surface);
    } else if (inputId == "nombre-artiste") {
      nbArtistes = content;
    } else if (inputId == "nouvelles-pieces") {
      nouvellesPieces = content;
    } else if (inputId == "duree-evenement") {
      if (content <= 7) {
        duree = 1;
      } else if (content > 7 && content <= 7 * 6) {
        duree = 1.2;
      } else if (content > 7 * 6) {
        duree = 1.4;
      }
    }

    totalDroitsExpo = ((nouvellesPieces * 2 + (140 - nouvellesPieces)) * surface) * duree / nbArtistes;
    totalDroitsExpo = totalDroitsExpo.toFixed(2);

    var totalJoursConception = +conception.querySelector(".total-jours").innerHTML;

    if (nbArtistes < 10 && totalJoursConception < 5) {
      showAlert(conception);
    } else {
      hideAlert(conception);
    }

    var totalJoursConceptionArtiste = parseFloat(conceptionArtiste.querySelector('.total-jours').innerHTML);
    
    updateForfaitCurateurice(nbArtistes, totalJoursConceptionArtiste, surface, nouvellesPieces);

    totalDroitsExpoEl.innerHTML = totalDroitsExpo;
    updateTotal(input);

  } else {
    alertNaN(input);
  }
}

function updateForfaitCurateurice(nbArtistes, jours, surface, nouvellesPieces) {
  var forfaitJours = jours + nbArtistes / 2;
  var forfait = document.querySelector('#forfait');
  var tauxCurateurice = calculatorCurateurice.querySelector('#taux-journalier').valueAsNumber;
  var prixForfaitJours = ((forfaitJours * tauxCurateurice) - (100 - nouvellesPieces * 1)) * surface;
  var prixJours = forfait.nextElementSibling.querySelector('span');
  forfait.value = forfaitJours;
  prixJours.innerHTML = prixForfaitJours;

  var part = calculatorCurateurice.querySelector('.conception');
  var inputs = part.querySelectorAll("input[type=number]");
  var lesPrixJours = part.querySelectorAll(".prix-jours span");
  var totalPartEl = part.querySelector(".total .price span");
  var totalJoursEl = part.querySelector(".total-jours");
  var lesPrix = [];
  var totalJours = 0;
  var totalPart = 0;
  inputs.forEach((input) => {
    updateSum(part, inputs, input, null, lesPrixJours, totalPartEl, totalJoursEl, lesPrix, totalJours, totalPart, null);
  });
}

function sumDays(part, maxArtistes, minJours, first) {
  var inputs = part.querySelectorAll("input[type=number]");
  var lesPrixJours = part.querySelectorAll(".prix-jours span");
  var totalPartEl = part.querySelector(".total .price span");
  var totalJoursEl = part.querySelector(".total-jours");
  var lesPrix = [];
  var totalJours = 0;
  var totalPart = 0;
  var alertCurateurice = calculatorCurateurice.querySelector('.alertCurateurice');

  inputs.forEach((input) => {
    updateSum(part, inputs, input, maxArtistes, lesPrixJours, totalPartEl, totalJoursEl, lesPrix, totalJours, totalPart, minJours, first);
    var parent = part.closest("article");
    if (first == true) {
      input.addEventListener("input", function (e) {
        alertCurateurice.classList.remove('visible');
        updateSum(part, inputs, input, maxArtistes, lesPrixJours, totalPartEl, totalJoursEl, lesPrix, totalJours, totalPart, minJours, first);
        if (part.classList.contains('conception') && parent.classList.contains("artiste")) {
          var surface;
          var content = document.querySelector('#surface-expo').value;
          if (surface <= 10) {
            surface = 1;
          } else if (content > 10 && content <= 50) {
            surface = 2;
          } else if (content > 50 && content <= 150) {
            surface = 4;
          } else if (content > 150) {
            surface = 6;
          }
          var nbArtistes = document.querySelector('#nombre-artiste').value
          var nouvellesPieces = document.querySelector('#nouvelles-pieces').value;
          var totalJours = parseFloat(part.querySelector('.total-jours').innerHTML);
          updateForfaitCurateurice(nbArtistes, totalJours, surface, nouvellesPieces);
        }
      });
    }
  });
}

function updateSum(part,inputs,input,maxArtistes,lesPrixJours,totalPartEl,totalJoursEl,lesPrix,totalJours,totalPart,minJours, first) {
  var content = input.valueAsNumber;
  var parent = part.closest("article");
  if (isNaN(content) != true) {
    if (parent.classList.contains("artiste")) {
      var total = part.classList.contains("mediation") ? content * tauxJournalierMediationArtiste : content * tauxJournalierArtiste;
    } else if (parent.classList.contains("curateurice")) {
      var total = part.classList.contains("mediation") ? content * tauxJournalierMediationCurateurice : content * tauxJournalierConceptionCurateurice;
    }
    var lePrixJour = input.nextElementSibling.querySelector("span");
    input.classList.remove("alert");
    lePrixJour.innerHTML = total.toFixed(2);
    for (let i = 0; i < lesPrixJours.length; i++) {
      lesPrix[i] = +lesPrixJours[i].innerText;
    }
    totalJours = [];
    for (let i = 0; i < inputs.length; i++) {
      var val = +inputs[i].value;
      totalJours[i] = val;
    }
    totalJours = totalJours.reduce((partialSum, a) => partialSum + a, 0);

    if (maxArtistes != null && minJours != null) {
      if (nbArtistes < maxArtistes && totalJours < minJours) {
        showAlert(part);
      } else {
        hideAlert(part);
      }
    }
    totalJoursEl.innerHTML = totalJours;

    totalPart = lesPrix.reduce((partialSum, a) => partialSum + a, 0);
    totalPartEl.innerHTML = totalPart.toFixed(2);
    updateTotal(input);
  } else {
    alertNaN(input);
  }
}

function sumTexts(textes, first) {
  var input = textes.querySelector("input");
  var totalPartEl = textes.querySelector(".total .price span");
  updateTexts(input, totalPartEl);
  if (first == true) {
    input.addEventListener("input", function (e) {
      updateTexts(input, totalPartEl);
    });
  }
}

function updateTexts(input, totalPartEl) {
  var content = input.valueAsNumber;
  var parent = input.closest("article");
  if (isNaN(content) != true) {
    if (parent.classList.contains("artiste")) {
      var total = content * prixSigneArtiste;
    } else if (parent.classList.contains("curateurice")) {
      var total = content * prixSigneCurateurice;
    }
    var lePrixTotal = input.nextElementSibling.querySelector("span");
    input.classList.remove("alert");
    lePrixTotal.innerHTML = total.toFixed(2);
    totalPartEl.innerHTML = total.toFixed(2);
    updateTotal(input);
  } else {
    alertNaN(input);
  }
}

function sumHorsRenum(horsRenumeration, first) {
  var inputs = horsRenumeration.querySelectorAll("input[type=number]");
  var totalPartEl = horsRenumeration.querySelector(".total .price span");
  var lesPrix = [];
  inputs.forEach((input) => {
    updateHorsRenum(inputs, input, totalPartEl, lesPrix);
    if (first == true) {
      input.addEventListener("input", function (e) {
        updateHorsRenum(inputs, input, totalPartEl, lesPrix);
      });
    }
  });
}

function updateHorsRenum(inputs, input, totalPartEl, lesPrix) {
  var content = input.valueAsNumber;
  if (isNaN(content) != true) {
    input.classList.remove("alert");
    for (let i = 0; i < inputs.length; i++) {
      lesPrix[i] = +inputs[i].value;
    }
    totalPart = lesPrix.reduce((partialSum, a) => partialSum + a, 0);
    totalPartEl.innerHTML = totalPart.toFixed(2);
    updateTotal(input);
  } else {
    alertNaN(input);
  }
}

function updateTotal(input) {
  var part = input.closest("article");
  var totalGeneralEl = part.querySelector("#total span.price");
  var totaux = part.querySelectorAll(".total .price span");
  var total = 0;
  totaux.forEach((totalEl) => {
    total += +totalEl.innerHTML;
  });
  totalGeneralEl.innerHTML = total.toFixed(2) + " €";
}

function showAlert(part) {
  var totalJoursEl = part.querySelector(".total-jours");
  var alertEl = part.querySelector("p.alert");
  var alertArtistesEl = alertEl.querySelector(".artistes");
  totalJoursEl.classList.add("alert");
  alertEl.classList.add("visible");
  alertArtistesEl.innerHTML = nbArtistes;
}

function hideAlert(part) {
  var totalJoursEl = part.querySelector(".total-jours");
  var alertEl = part.querySelector("p.alert");
  totalJoursEl.classList.remove("alert");
  alertEl.classList.remove("visible");
}

function alertNaN(element) {
  element.classList.add("alert");
}