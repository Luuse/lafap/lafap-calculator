var main = document.querySelector('main');
var sections = document.querySelectorAll('main > section');
var hash = window.location.hash;

if (hash) {
  var section = hash.split('++')[0];
  var tab = hash.split('++')[1];
  var sectionToOpen = document.querySelector(section);
  if (sectionToOpen) {
    sections.forEach(section => {
      section.classList.remove('clicked');
    })
    sectionToOpen.classList.add('clicked');
    setRowsAndColumns(sectionToOpen);
  }
  if (tab) {
    var feuilletsButtons = sectionToOpen.querySelectorAll('input[type=radio]');
    feuilletsButtons.forEach(button => {
      if (button.id == tab) {
        button.checked = true;
      }
    })

  }
}

var links = document.querySelectorAll('article a');
links.forEach(link => {
  link.addEventListener('click', function(e) {
    var url = link.getAttribute('href');
    if (url.includes('/processwire/')) {
      e.stopPropagation();
      e.preventDefault();
      var part = url.split('/')[2];
      var tab = url.split('/')[3];
      var sectionToOpen = document.querySelector('#' + part);
      var tabName = part + '_' + tab;
      var hash = tab ? part + '++' + tabName : part;
      window.location.hash = hash;
      if (sectionToOpen) {
        sections.forEach(section => {
          section.classList.remove('clicked');
        })
        sectionToOpen.classList.add('clicked');
        setRowsAndColumns(sectionToOpen);
      }
      if (tab) {
        var feuilletsButtons = sectionToOpen.querySelectorAll('input[type=radio]');
        feuilletsButtons.forEach(button => {
          if (button.id == tabName) {
            button.checked = true;
          }
        })
      }
    }
  })
})

sections.forEach(section => {

  section.addEventListener('click', function() {

    var sectionId = section.id;

    if(!section.classList.contains('clicked')) {
      window.location.hash = sectionId;
      sections.forEach(section => {
        section.classList.remove('clicked');
        section.scrollTop = 0;
      })
      
      section.classList.add('clicked');
  
      setRowsAndColumns(section);
    }

  })
})
var feuilletsButtons = document.querySelectorAll('.feuillet label');

feuilletsButtons.forEach(button => {
  
  button.addEventListener('click', function() {
    console.log('click')
    var buttonId = button.getAttribute('for');
    var sectionId = button.closest('section.container').id;
    var newHash = sectionId + '++' + buttonId;
    window.location.hash = newHash;
  })

})


function setRowsAndColumns(section) {
  
  var sectionRow = section.dataset.row;
  var sectionCol = section.dataset.column;

  if (sectionRow == 1) {
    main.style.gridTemplateRows = '86% 14%'
  } else {
    main.style.gridTemplateRows = '14% 86%'
  }

  if (sectionCol == 1) {
    main.style.gridTemplateColumns = '86% 7% 7%'
  } else if (sectionCol == 2) {
    main.style.gridTemplateColumns = '7% 86% 7%'
  } else {
    main.style.gridTemplateColumns = '7% 7% 86%'
  }

}