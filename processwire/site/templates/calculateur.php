<?php
    $partAlert = $pages->get('template=calculateur_artiste');
    $alertValeur = $partAlert->getLanguageValue($lang, 'alerte_valeur_nombre');
    $alertNbArtistes = $partAlert->getLanguageValue($lang, 'alerte_nombre_artiste');
    $alertNbArtistes = $alertNbArtistes ? str_replace('[', '<span class="artistes">', $alertNbArtistes): '';
    $alertNbArtistes = $alertNbArtistes ? str_replace(']', '</span>', $alertNbArtistes) : '';
    $alertCurateurice = $partAlert->getLanguageValue($lang, 'alerte_formulaire_curateurice');
    $printButton = $partAlert->getLanguageValue($lang, 'imprimer');
    $printInfos = $partAlert->getLanguageValue($lang, 'imprimer_informations');
?>
<div class="feuille">
    <h1 class="hidden"> <?= $container->getLanguageValue($lang, 'title'); ?></h1>
    <h1><?= $container->getLanguageValue($lang, 'title'); ?></h1>
    <article>
        <h2><?= $part->getLanguageValue($lang, 'title'); ?></h2>
        <?php
            if ($lang == 'fr' || $lang == 'default') {
                echo rft_fr($part->getLanguageValue($lang, 'text'));
            } else {
                echo rft_en($part->getLanguageValue($lang, 'text'));
            }
        ?>
    </article>

    <?php if ($part->template == 'calculateur_artiste') : ?>
        <article class="calculator artiste">
          <form action="">
          <div class="part taux">
                <h3>
                    <?= $part->getLanguageValue($lang, 'part_title_taux') ?>
                    <?php if ($part->infobulle_taux) : ?>
                        <div class="infos">
                            <p class="question-mark"><span>i</span></p>
                            <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_taux')) : rft_en($part->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                        </div>
                    <?php endif; ?>
                </h3>
                <ul>
                    <li>
                        <label for="taux-journalier"><?= $part->getLanguageValue($lang, 'taux_journalier') ?></label>
                        <input type="number" min="<?= $part->taux_journalier_minimum ?>" value="<?= $part->taux_journalier_minimum ?>" name="" id="taux-journalier">
                        <?php if ($part->taux_journalier_infobulle) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'taux_journalier_infobulle')) : rft_en($part->getLanguageValue($lang, 'taux_journalier_infobulle')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <li>
                        <label for="taux-journalier-mediation"><?= $part->getLanguageValue($lang, 'taux_forfaitaire') ?></label>
                        <input type="number" min="<?= $part->taux_forfaitaire_minimum ?>" value="<?= $part->taux_forfaitaire_minimum ?>" name="" id="taux-journalier-mediation">
                        <?php if ($part->taux_forfaitaire_infobulle) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'taux_forfaitaire_infobulle')) : rft_en($part->getLanguageValue($lang, 'taux_forfaitaire_infobulle')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <li>
                        <label for="prix-signe"><?= $part->getLanguageValue($lang, 'prix_signe') ?></label>
                        <input type="number" min="<?= $part->prix_signe_minimum ?>" value="<?= $part->prix_signe_minimum ?>" name="" id="prix-signe">
                        <?php if ($part->prix_signe_infobulle) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'prix_signe_infobulle')) : rft_en($part->getLanguageValue($lang, 'prix_signe_infobulle')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                </ul>
            </div>

            <div class="part droits">
                <h3>
                    <?= $part->getLanguageValue($lang, 'part_title_droits') ?>
                    <?php if ($part->infobulle_droits) : ?>
                        <div class="infos">
                            <p class="question-mark"><span>i</span></p>
                            <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_droits')) : rft_en($part->getLanguageValue($lang, 'infobulle_droits')) ?></p>
                        </div>
                    <?php endif; ?>
                </h3>
              <ul>
                <li>
                  <label for="surface-expo"><?= $part->getLanguageValue($lang, 'surface_d_exposition') ?></label>
                  <input type="number" min="1" value="1" name="" id="surface-expo">
                  <?php if ($part->surface_d_exposition_infobulle) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'surface_d_exposition_infobulle')) : rft_en($part->getLanguageValue($lang, 'surface_d_exposition_infobulle')) ?></p>
                    </div>
                    <?php endif; ?>
                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                </li>
                <li>
                  <label for="nombre-artiste"><?= $part->getLanguageValue($lang, 'nombre_d_artiste') ?></label>
                  <input type="number" min="1" value="1" name="" id="nombre-artiste">
                  <?php if ($part->nombre_d_artiste_infobulle) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'nombre_d_artiste_infobulle')) : rft_en($part->getLanguageValue($lang, 'nombre_d_artiste_infobulle')) ?></p>
                    </div>
                    <?php endif; ?>
                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                </li>
                <li>
                  <label for="nouvelles-pieces"><?= $part->getLanguageValue($lang, 'pourcentage_de_nouvelles_pieces') ?></label>
                  <input type="number" min="0" value="0" max="100" name="" id="nouvelles-pieces">
                  <?php if ($part->pourcentage_de_nouvelles_pieces_infobulle) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'pourcentage_de_nouvelles_pieces_infobulle')) : rft_en($part->getLanguageValue($lang, 'pourcentage_de_nouvelles_pieces_infobulle')) ?></p>
                    </div>
                    <?php endif; ?>
                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                </li>
                <li>
                  <label for="duree-evenement"><?= $part->getLanguageValue($lang, 'duree_de_l_evenement') ?></label>
                  <input type="number" min="0" value="1" name="" id="duree-evenement">
                  <?php if ($part->duree_de_l_evenement_infobulle) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'duree_de_l_evenement_infobulle')) : rft_en($part->getLanguageValue($lang, 'duree_de_l_evenement_infobulle')) ?></p>
                    </div>
                    <?php endif; ?>
                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                </li>
              </ul>
              <div class="total">
                <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_droits')) ?></span> <span class="price"><span>0.00</span> €</span></p>
              </div>
            </div>


            <div class="part conception">
              <h3>
                <?= $part->getLanguageValue($lang, 'part_title_conception') ?>
                <?php if ($part->infobulle_conception) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_conception')) : rft_en($part->getLanguageValue($lang, 'infobulle_conception')) ?></p>
                    </div>
                <?php endif; ?>
              </h3>
              <ul>
                <?php foreach ($part->items_conception as $i => $item) :
                    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                    if ($i + 1 < count($part->items_conception)) :
                    ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                        <input type="number" min="0" value="0" id="<?= $slug ?>">
                        <p class="prix-jours"><span>0.00</span> €</p>
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php  elseif ($i + 1 == count($part->items_conception)) : ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?>
                        <input type="text" class="label-autre"></label>
                        <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                        <p class="prix-jours"><span>0.00</span> €</p>
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php
                    endif;
                endforeach; ?>
              </ul>
              <div class="total">
                <p class="cadre-total-jours"><span>Total jours</span> <span class="total-jours">0</span></p>
                <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_conception')) ?></span> <span class="price"><span>0.00</span> €</span></p>
                <p class="alert"><?= $alertNbArtistes ?></p>
              </div>
            </div>

            <div class="part mediation">
              <h3>
                <?= $part->getLanguageValue($lang, 'part_title_mediation') ?>
                <?php if ($part->infobulle_mediation) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_mediation')) : rft_en($part->getLanguageValue($lang, 'infobulle_mediation')) ?></p>
                    </div>
                <?php endif; ?>
              </h3>
              <ul>
                <?php foreach ($part->items_mediation as $i => $item) :
                    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                    if ($i + 1 < count($part->items_mediation)) :
                    ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                        <input type="number" min="0" value="0" id="<?= $slug ?>">
                        <p class="prix-jours"><span>0.00</span> €</p>
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php  elseif ($i + 1 == count($part->items_mediation)) : ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?>
                        <input type="text" class="label-autre"></label>
                        <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                        <p class="prix-jours"><span>0.00</span> €</p>
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php
                    endif;
                endforeach; ?>
              </ul>
              <div class="total">
                <p class="cadre-total-jours"><span>Total jours</span> <span class="total-jours">0</span></p>
                <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_mediation')) ?></span> <span class="price"><span>0.00</span> €</span></p>
              </div>
            </div>

            <div class="part textes">
              <h3>
              <?= $part->getLanguageValue($lang, 'part_title_textes') ?>
                <?php if ($part->infobulle_textes) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_textes')) : rft_en($part->getLanguageValue($lang, 'infobulle_textes')) ?></p>
                    </div>
                <?php endif; ?>
              </h3>
              <ul>
                <li>
                  <label for="nombre-signes"><?= $part->getLanguageValue($lang, 'nombre_de_signe_titre') ?></label>
                  <input type="number" min="0" value="0" name="" id="nombre-signes">
                  <p class="prix-jours"><span>0.00</span> €</p>
                  <?php if ($part->nombre_de_signe_infobulle) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'nombre_de_signe_infobulle')) : rft_en($part->getLanguageValue($lang, 'nombre_de_signe_infobulle')) ?></p>
                    </div>
                    <?php endif; ?>
                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                </li>
              </ul>
              <div class="total">
                <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_textes')) ?></span> <span class="price"><span>0.00</span> €</span></p>
              </div>
            </div>

            <div class="part horsRenumeration">
              <h3>
                <?= $part->getLanguageValue($lang, 'part_title_hors_renumeration') ?>
                <?php if ($part->infobulle_hors_renumeration) : ?>
                    <div class="infos">
                        <p class="question-mark"><span>i</span></p>
                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_hors_renumeration')) : rft_en($part->getLanguageValue($lang, 'infobulle_hors_renumeration')) ?></p>
                    </div>
                <?php endif; ?>
              </h3>
              <ul>
                <?php foreach ($part->items_hors_renumeration as $i => $item) :
                    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                    if ($i + 1 < count($part->items_hors_renumeration)) :
                    ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                        <input type="number" min="0" value="0" id="<?= $slug ?>">
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php  elseif ($i + 1 == count($part->items_hors_renumeration)) : ?>
                    <li>
                        <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?><input type="text" class="label-autre"></label>
                        <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                        <?php if ($item->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                            <?php endif; ?>
                            <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                    </li>
                    <?php
                    endif;
                endforeach; ?>
              </ul>
              <div class="total">
                <p><span>Total jours</span> <span class="total-jours">0</span></p>
                <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_hors_renumeration')) ?></span> <span class="price"><span>0.00</span> €</span></p>
              </div>
            </div>

        </form>

        <p id="total" class="total"><span>TOTAL HT</span> <span class="price">0.00 €</span></p>

        <div class="print">
            <button><?= $printButton ?></button>
            <p><?= $printInfos ?></p>
        </div>

    </article>
    <?php elseif ($part->template == 'calculateur_curateurice') : ?>
        <article class="calculator curateurice">
            <p class="alert alertCurateurice visible"><?= $alertCurateurice ?></p>

            <form action="">
                <div class="part taux">
                    <h3>
                        <?= $part->getLanguageValue($lang, 'part_title_taux') ?>
                        <?php if ($part->infobulle_taux) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_taux')) : rft_en($part->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                            </div>
                        <?php endif; ?>
                    </h3>
                    <ul>
                        <li>
                            <label for="taux-journalier"><?= $part->getLanguageValue($lang, 'taux_journalier') ?></label>
                            <input type="number" min="<?= $part->taux_journalier_minimum ?>" value="<?= $part->taux_journalier_minimum ?>" name="" id="taux-journalier">
                            <?php if ($part->taux_journalier_infobulle) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'taux_journalier_infobulle')) : rft_en($part->getLanguageValue($lang, 'taux_journalier_infobulle')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                        </li>
                        <li>
                            <label for="taux-journalier-mediation"><?= $part->getLanguageValue($lang, 'taux_forfaitaire') ?></label>
                            <input type="number" min="<?= $part->taux_forfaitaire_minimum ?>" value="<?= $part->taux_forfaitaire_minimum ?>" name="" id="taux-journalier-mediation">
                            <?php if ($part->taux_forfaitaire_infobulle) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'taux_forfaitaire_infobulle')) : rft_en($part->getLanguageValue($lang, 'taux_forfaitaire_infobulle')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                        </li>
                        <li>
                            <label for="prix-signe"><?= $part->getLanguageValue($lang, 'prix_signe') ?></label>
                            <input type="number" min="<?= $part->prix_signe_minimum ?>" value="<?= $part->prix_signe_minimum ?>" name="" id="prix-signe">
                            <?php if ($part->prix_signe_infobulle) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'prix_signe_infobulle')) : rft_en($part->getLanguageValue($lang, 'prix_signe_infobulle')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                        </li>
                    </ul>
                </div>

                <div class="part conception">
                    <h3>
                        <?= $part->getLanguageValue($lang, 'part_title_conception') ?>
                        <?php if ($part->infobulle_conception) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_conception')) : rft_en($part->getLanguageValue($lang, 'infobulle_conception')) ?></p>
                            </div>
                        <?php endif; ?>
                    </h3>
                    <ul>
                        <li>
                            <label for="forfait"><?= $part->getLanguageValue($lang, 'forfait_de_base') ?></label>
                            <input type="number" min="0" value="0" name="" disabled="disabled" id="forfait">
                            <p class="prix-jours"><span>0.00</span> €</p>
                            <?php if ($part->infobulle_forfait_de_base) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_forfait_de_base')) : rft_en($part->getLanguageValue($lang, 'infobulle_forfait_de_base')) ?></p>
                                </div>
                                <?php endif; ?>
                        </li>
                        <?php foreach ($part->items_conception as $i => $item) :
                            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                            if ($i + 1 < count($part->items_conception)) :
                            ?>
                            <li>
                                <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                                <input type="number" min="0" value="0" id="<?= $slug ?>">
                                <p class="prix-jours"><span>0.00</span> €</p>
                                <?php if ($item->infobulle_taux) : ?>
                                    <div class="infos">
                                        <p class="question-mark"><span>i</span></p>
                                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                    </div>
                                    <?php endif; ?>
                                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                            </li>
                            <?php  elseif ($i + 1 == count($part->items_conception)) : ?>
                            <li>
                                <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?><input type="text" class="label-autre"></label>
                                <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                                <p class="prix-jours"><span>0.00</span> €</p>
                                <?php if ($item->infobulle_taux) : ?>
                                    <div class="infos">
                                        <p class="question-mark"><span>i</span></p>
                                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                    </div>
                                    <?php endif; ?>
                                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                            </li>
                            <?php
                            endif;
                        endforeach; ?>
                    </ul>
                    <div class="total">
                        <p class="cadre-total-jours"><span>Total jours</span> <span class="total-jours">0</span></p>
                        <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_conception')) ?></span> <span class="price"><span>0.00</span> €</span></p>
                        <p class="alert"><?= $alertNbArtistes ?></p>
                    </div>
                    </div>

                    <div class="part mediation">
                    <h3>
                        <?= $part->getLanguageValue($lang, 'part_title_mediation') ?>
                        <?php if ($part->infobulle_mediation) : ?>
                            <div class="infos">
                                <p class="question-mark"><span>i</span></p>
                                <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_mediation')) : rft_en($part->getLanguageValue($lang, 'infobulle_mediation')) ?></p>
                            </div>
                        <?php endif; ?>
                    </h3>
                    <ul>
                        <?php foreach ($part->items_mediation as $i => $item) :
                            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                            if ($i + 1 < count($part->items_mediation)) :
                            ?>
                            <li>
                                <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                                <input type="number" min="0" value="0" id="<?= $slug ?>">
                                <p class="prix-jours"><span>0.00</span> €</p>
                                <?php if ($item->infobulle_taux) : ?>
                                    <div class="infos">
                                        <p class="question-mark"><span>i</span></p>
                                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                    </div>
                                    <?php endif; ?>
                                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                            </li>
                            <?php  elseif ($i + 1 == count($part->items_mediation)) : ?>
                            <li>
                                <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?><input type="text" class="label-autre"></label>
                                <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                                <p class="prix-jours"><span>0.00</span> €</p>
                                <?php if ($item->infobulle_taux) : ?>
                                    <div class="infos">
                                        <p class="question-mark"><span>i</span></p>
                                        <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                    </div>
                                    <?php endif; ?>
                                    <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                            </li>
                            <?php
                            endif;
                        endforeach; ?>
                    </ul>
                    <div class="total">
                        <p class="cadre-total-jours"><span>Total jours</span> <span class="total-jours">0</span></p>
                        <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_mediation')) ?></span> <span class="price"><span>0.00</span> €</span></p>
                    </div>
                    </div>

                    <div class="part textes">
                        <h3>
                        <?= $part->getLanguageValue($lang, 'part_title_textes') ?>
                            <?php if ($part->infobulle_textes) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_textes')) : rft_en($part->getLanguageValue($lang, 'infobulle_textes')) ?></p>
                                </div>
                            <?php endif; ?>
                        </h3>
                        <ul>
                            <li>
                            <label for="nombre-signes"><?= $part->getLanguageValue($lang, 'nombre_de_signe_titre') ?></label>
                            <input type="number" min="0" value="0" name="" id="nombre-signes">
                            <p class="prix-jours"><span>0.00</span> €</p>
                            <?php if ($part->nombre_de_signe_infobulle) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'nombre_de_signe_infobulle')) : rft_en($part->getLanguageValue($lang, 'nombre_de_signe_infobulle')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                            </li>
                        </ul>
                        <div class="total">
                            <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_textes')) ?></span> <span class="price"><span>0.00</span> €</span></p>
                        </div>
                    </div>
                    <div class="part horsRenumeration">
                <h3>
                    <?= $part->getLanguageValue($lang, 'part_title_hors_renumeration') ?>
                    <?php if ($part->infobulle_hors_renumeration) : ?>
                        <div class="infos">
                            <p class="question-mark"><span>i</span></p>
                            <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($part->getLanguageValue($lang, 'infobulle_hors_renumeration')) : rft_en($part->getLanguageValue($lang, 'infobulle_hors_renumeration')) ?></p>
                        </div>
                    <?php endif; ?>
                </h3>
                <ul>
                    <?php foreach ($part->items_hors_renumeration as $i => $item) :
                        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $item->item)));
                        if ($i + 1 < count($part->items_hors_renumeration)) :
                        ?>
                        <li>
                            <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?></label>
                            <input type="number" min="0" value="0" id="<?= $slug ?>">
                            <?php if ($item->infobulle_taux) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                        </li>
                        <?php  elseif ($i + 1 == count($part->items_hors_renumeration)) : ?>
                        <li>
                            <label for="<?= $slug ?>"><?= $item->getLanguageValue($lang, 'item') ?><input type="text" class="label-autre"></label>
                            <input type="number" min="0" value="0" name="" id="<?= $slug ?>">
                            <?php if ($item->infobulle_taux) : ?>
                                <div class="infos">
                                    <p class="question-mark"><span>i</span></p>
                                    <p class="text"><?= ($lang == 'fr' || $lang == 'default') ? rft_fr($item->getLanguageValue($lang, 'infobulle_taux')) : rft_en($item->getLanguageValue($lang, 'infobulle_taux')) ?></p>
                                </div>
                                <?php endif; ?>
                                <span class="alert" data-value="⚠ <?= $alertValeur ?>"></span>
                        </li>
                        <?php
                        endif;
                    endforeach; ?>
                </ul>
                <div class="total">
                    <p><span>Total jours</span> <span class="total-jours">0</span></p>
                    <p><span>Total <?= strtolower($part->getLanguageValue($lang, 'part_title_hors_renumeration')) ?></span> <span class="price"><span>0.00</span> €</span></p>
                </div>
                </div>

            </form>

            <p id="total" class="total"><span>TOTAL HT</span> <span class="price">0.00 €</span></p>

            <div class="print">
                <button><?= $printButton ?></button>
                <p><?= $printInfos ?></p>
            </div>

        </article>
    <?php endif ?>
</div>
