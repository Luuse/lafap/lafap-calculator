<?php $lang = $_GET ? $_GET['lang'] : 'default'; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>/styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>/styles/print.css"/>
    <title>LaFap - Calculateur</title>
    <link rel="shortcut icon" href="<?= $config->urls->templates ?>/images/favicon.ico" type="image/x-icon">
</head>