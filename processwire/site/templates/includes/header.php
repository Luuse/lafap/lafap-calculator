<body>
  <header>
    <ul>
      <li id="logo"><img src="<?= $page->logo->src ?>" alt=""></li>
      <li id="title"><?= $page->getLanguageValue($lang, 'title') ?></li>
    </ul>
    <nav>
      <ul>
      <?php
      foreach($languages as $language) {
        $lang_title = $language->title;
        $lang_name = $language->name;
        $active = $lang == $lang_name ? 'active' : '';
        echo '<li class="' . $lang_title . ' ' . $active . '"><a href="?lang=' . $lang_name . '">' . $lang_title . '</a></li>';
      }
      ?>
      </ul>
    </nav>
  </header>
<main>