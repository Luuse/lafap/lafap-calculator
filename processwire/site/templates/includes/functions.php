<?php 

function rft_fr($content) {
  $thinsp = '<span class="thinsp">&nbsp;</span>';
  $regex = array(
          # On commence par retirer tout les espaces avant de mettre les espaces fines

    // "/(?![^<]*>)\s*;/"	=>	    ';',	                # espace point-virgule		            => point-virgule
    "/(?![^<]*>)«\s*/"	=>	    '«',	                # guillement français ouvrant espace    => guillement français ouvrant
    "/(?![^<]*>)\s*»/"	=>	    '»',	                # espace guillement français fermant    => guillement français fermant
    "/(?![^<]*>)\s*:/"	=>	    ':',	                # espace deux points	                => deux points
    "/(?![^<]*>)\s*!/"	=>	    '!',	                # espace point d'exclamation	        => point d'exclamation
    "/(?![^<]*>)\s*\?/"	=>	    '?',	                # espace point d'interrogation	        => point d'interrogation

        # On rajoute une balise .thinsp entourant une espace insécable.
        # La gestion de la largeur de l'espace se fait en css avec la propriété letter-spacing
    "/(?![^<]*>) ;/"    =>	'<span class="thinsp">&nbsp;</span>;',  # point virgule        => narrow non breaking space point virgule
    "/(?![^<]*>):/"	      =>	'<span class="thinsp">&nbsp;</span>:',	# deux points	           => narrow non breaking space deux points
    "/(?![^<]*>)«/"	      =>	'«<span class="thinsp">&nbsp;</span>',	# guillement ouvrant       => guillemet ouvrante narrow non breaking space
    "/(?![^<]*>)»/"	      =>	'<span class="thinsp">&nbsp;</span>»',	# guillement fermant       => narrow non breaking space  guillemet fermante
    "/(?![^<]*>)!/"	      =>	'<span class="thinsp">&nbsp;</span>!',	# point d'exclamation      => narrow non breaking space point d'exclamation
    "/(?![^<]*>)\?/"	  =>	'<span class="thinsp">&nbsp;</span>?',	# point d'interrogation => narrow non breaking space  point d'interrogation
        # Caractères spéciaux
    "/(?![^<]*>)& /"	  =>	'&amp; ',	            # &			                           => &amp;
    "/(?![^<]*>)oe/"	  =>	'&#339;',	            # oe			                       => œ
    "/(?![^<]*>)ae/"	  =>	'&#230;',	            # ae			                       => æ
    "/(?![^<]*>)OE/"	  =>	'&#338;',	            # OE			                       => Œ
    "/(?![^<]*>)AE/"	  =>	'&#198;',	            # AE			                       => Æ
    "/(?![^<]*>)\.\.\./"  =>    '&#8230;',	            # AE			                       => Æ
  );
    
  foreach ($regex as $input => $output) {
    $content = preg_replace($input, $output, $content);
    }
    return $content;
  }


function rft_en($content) {
  $regex = array(
          # On commence par retirer tout les espaces avant de mettre les espaces fines

    // "/(?![^<]*>)\s*;/"	=>	    ';',	                # espace point-virgule		            => point-virgule
    "/(?![^<]*>)«\s*/"	=>	    '«',	                # guillement français ouvrant espace    => guillement français ouvrant
    "/(?![^<]*>)\s*»/"	=>	    '»',	                # espace guillement français fermant    => guillement français fermant
    "/(?![^<]*>)\s*:/"	=>	    ':',	                # espace deux points	                => deux points
    "/(?![^<]*>)\s*!/"	=>	    '!',	                # espace point d'exclamation	        => point d'exclamation
    "/(?![^<]*>)\s*\?/"	=>	    '?',	                # espace point d'interrogation	        => point d'interrogation
  );
    
  foreach ($regex as $input => $output) {
    $content = preg_replace($input, $output, $content);
    }
    return $content;
  }